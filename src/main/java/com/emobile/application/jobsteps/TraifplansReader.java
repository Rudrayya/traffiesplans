package com.emobile.application.jobsteps;



import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;

import com.emobile.application.entity.Traifplans;



public class TraifplansReader extends FlatFileItemReader<Traifplans>{
       public TraifplansReader(org.springframework.core.io.Resource resource) {
		
		super();
		
		setResource((org.springframework.core.io.Resource) resource);
		
		DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
		lineTokenizer.setNames(new String[] { "planId", "planName", "planDesc", "planAmount" ,"planServiceCharges","planValidity","planStatus"});
		lineTokenizer.setDelimiter(",");
	    lineTokenizer.setStrict(false);
	    
	    BeanWrapperFieldSetMapper<Traifplans> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(Traifplans.class);

		DefaultLineMapper<Traifplans> defaultLineMapper = new DefaultLineMapper<>();
		defaultLineMapper.setLineTokenizer(lineTokenizer);
		defaultLineMapper.setFieldSetMapper(fieldSetMapper);
		setLineMapper(defaultLineMapper);
       }
}
