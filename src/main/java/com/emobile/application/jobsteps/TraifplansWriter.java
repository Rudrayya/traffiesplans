package com.emobile.application.jobsteps;


import java.util.List;

import javax.transaction.Transactional;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.emobile.application.entity.Traifplans;
import com.emobile.application.repository.TraifplansRepository;



@Component
public class TraifplansWriter implements ItemWriter<Traifplans>
{
	
	@Autowired
	private  TraifplansRepository traifRepo;


	@Override
	@Transactional
	public void write(List<? extends Traifplans> traifplans) throws Exception {
		traifRepo.saveAll(traifplans);
		
	}
}
