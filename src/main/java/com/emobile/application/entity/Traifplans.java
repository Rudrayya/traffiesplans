package com.emobile.application.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


import lombok.Data;

@Data
@Entity
public class Traifplans 
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "planId")
	private Integer planId;
	
	@Column(name = "planName")
	private String planName;
	
	@Column(name = "planDesc")
	private String planDesc;
	
	@Column(name = "planAmount")
	private String planAmount;
	
	@Column(name = "planServiceCharges")
	private String planServiceCharges;
	
	@Column(name = "planValidity")
	private String planValidity;
	
	@Column(name = "planStatus")
	private String planStatus;

	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planId) {
		this.planId = planId;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getPlanDesc() {
		return planDesc;
	}

	public void setPlanDesc(String planDesc) {
		this.planDesc = planDesc;
	}

	public String getPlanAmount() {
		return planAmount;
	}

	public void setPlanAmount(String planAmount) {
		this.planAmount = planAmount;
	}

	public String getPlanServiceCharges() {
		return planServiceCharges;
	}

	public void setPlanServiceCharges(String planServiceCharges) {
		this.planServiceCharges = planServiceCharges;
	}

	public String getPlanValidity() {
		return planValidity;
	}

	public void setPlanValidity(String planValidity) {
		this.planValidity = planValidity;
	}

	public String getPlanStatus() {
		return planStatus;
	}

	public void setPlanStatus(String planStatus) {
		this.planStatus = planStatus;
	}
}
