package com.emobile.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.emobile.application.entity.Traifplans;

public interface TraifplansRepository extends JpaRepository<Traifplans, Integer>{

}
